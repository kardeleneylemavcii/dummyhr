import React from 'react';

const EmployeeDialog = ({ selectedEmployee, closeModal, handleDelete, setIsEditing }) => {
  return (
    <div style={styles.modal}>
      <div style={styles.modalContent}>
        <button onClick={closeModal} style={styles.closeButton}>X</button>
        <div style={{ textAlign: 'center' }}>
          <h2>{selectedEmployee.employee_name}</h2>
          <p><strong>Age:</strong> {selectedEmployee.employee_age}</p>
          <p><strong>Salary:</strong> ${selectedEmployee.employee_salary}</p>
          <div style={{ marginTop: '20px', display: 'flex', justifyContent: 'center' }}>
            <button
              style={{ ...styles.buttonContainerButton, backgroundColor: 'red' }}
              onClick={handleDelete}
            >
              Delete
            </button>
            <button
              style={{ ...styles.buttonContainerButton, marginRight: '10px' }}
              onClick={() => setIsEditing(true)}
            >
              Edit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

const styles = {
  modal: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    backgroundColor: '#fff',
    padding: '20px',
    borderRadius: '8px',
    position: 'relative',
    width: '30%',
    height: '28%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingBottom: '2%',
  },
  closeButton: {
    position: 'absolute',
    top: '10px',
    right: '10px',
    backgroundColor: 'transparent',
    border: 'none',
    fontSize: '20px',
    cursor: 'pointer',
  },
  buttonContainerButton: {
    padding: '10px 20px',
    fontSize: '16px',
    backgroundColor: '#007bff',
    color: 'white',
    border: 'none',
    borderRadius: '4px',
    cursor: 'pointer',
    marginRight: '10px',
  },
};

export default EmployeeDialog;
