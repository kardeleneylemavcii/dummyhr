const EmployeeCard = ({ employee, openEmployeeDetail }) => {
    return (
      <div style={styles.card} onClick={() => openEmployeeDetail(employee)}>
        <h2>{employee.employee_name}</h2>
        <p>Age: {employee.employee_age}</p>
        <p>Salary: ${employee.employee_salary}</p>
      </div>
    );
  };
  
  const styles = {
    card: {
      border: '1px solid #ddd',
      borderRadius: '8px',
      padding: '20px',
      boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
      width: '200px',
      height: '150px',
      textAlign: 'center',
      textDecoration: 'none',
      color: 'inherit',
      display: 'block',
      cursor: 'pointer',
    },
  };
  
  export default EmployeeCard;