"use client";
import { useState } from 'react';
import Header from './components/Header'
import EmployeeList from './list/EmployeeList';
import EmployeeCreate from './create/EmployeeCreate';

// Buton kart bileşeni
const ButtonCard = ({ onClick, text }) => {
  return (
    <div style={styles.card} onClick={onClick}>
      <h2>{text}</h2>
    </div>
  );
};

export default function Home() {
  const [view, setView] = useState(null);

  return (
    <div>
      <Header/>
      {view === null && (
        <div style={styles.buttonContainer}>
          <ButtonCard onClick={() => setView('list')} text="View Employee List" />
          <ButtonCard onClick={() => setView('create')} text="Create New Employee" />
        </div>
      )}

      {view === 'list' && <EmployeeList />}
      {view === 'create' && <EmployeeCreate />}
    </div>
  );
}

const styles = {
  buttonContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15%',
  },
  card: {
    border: '1px solid #ddd',
    borderRadius: '8px',
    padding: '20px',
    boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
    width: '200px',
    height: '150px',
    textAlign: 'center',
    margin: '10px', // Butonlar arasında boşluk
    cursor: 'pointer',
  },
};
