import { useState, useEffect } from 'react';
import axios from '../lib/axios';
import EmployeeUpdate from '../components/EmployeeUpdate';
import EmployeeDialog from '../components/EmployeeDialog';
import EmployeeCard from '../components/EmployeeCard';

const EmployeeList = () => {
  const [employees, setEmployees] = useState([]);
  const [selectedEmployee, setSelectedEmployee] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [dialog, setDialog] = useState({ isOpen: false, message: '' });

  useEffect(() => {
    axios.get('/employees')
      .then(response => {
        setEmployees(response.data.data);
      })
      .catch(error => {
        console.error(error);
        setDialog({
          isOpen: true,
          message: 'Error fetching employees!',
        });
      });
  }, []);

  const openEmployeeDetail = (employee) => {
    setSelectedEmployee(employee);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
    setSelectedEmployee(null);
    setIsEditing(false);
  };

  const handleDelete = async () => {
    try {
      await axios.delete(`/delete/${selectedEmployee.id}`);
      closeModal();
      setEmployees(employees.filter(emp => emp.id !== selectedEmployee.id));
      setDialog({
        isOpen: true,
        message: 'Employee deleted successfully!',
      });
    } catch (error) {
      console.error(error);
      setDialog({
        isOpen: true,
        message: 'Error deleting employee!',
      });
    }
  };

  const handleSave = (updatedEmployee) => {
    setEmployees(employees.map(emp =>
      emp.id === updatedEmployee.id ? updatedEmployee : emp
    ));
    closeModal();
  };

  const closeDialog = () => {
    setDialog({ isOpen: false, message: '' });
  };

  return (
    <div>
      <h1 style={{ textAlign: 'center' }}>Employee List</h1>
      <div style={styles.cardContainer}>
        {employees.map(employee => (
          <EmployeeCard 
            key={employee.id}
            employee={employee}
            openEmployeeDetail={openEmployeeDetail}
          />
        ))}
      </div>
      {isModalOpen && (
        isEditing ? (
          <EmployeeUpdate
            selectedEmployee={selectedEmployee}
            closeModal={closeModal}
            onSave={handleSave}
            setIsEditing={setIsEditing}
          />
        ) : (
          <EmployeeDialog
            selectedEmployee={selectedEmployee}
            closeModal={closeModal}
            handleDelete={handleDelete}
            setIsEditing={setIsEditing}
          />
        )
      )}
      {dialog.isOpen && (
        <div className="dialogContainer">
          <p className="dialogContainerP">{dialog.message}</p>
          <button className="dialogContainerButton" onClick={closeDialog}>Close</button>
        </div>
      )}
    </div>
  );
};

const styles = {
  cardContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    gap: '20px',
  },
};

export default EmployeeList;
