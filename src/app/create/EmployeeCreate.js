import { useState } from 'react';
import axios from '../lib/axios';
import '../css/EmployeeCreate.css';

const EmployeeCreate = () => {
  const [name, setName] = useState('');
  const [salary, setSalary] = useState('');
  const [age, setAge] = useState('');
  const [dialog, setDialog] = useState({ isOpen: false, message: '' });

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('/create', {
        name,
        salary,
        age,
      });
      setDialog({
        isOpen: true,
        message: 'Employee created successfully!',
      });
      console.log('Employee created:', response.data);
    } catch (error) {
      console.error('Error creating employee:', error);
      if (error) {
        setDialog({
          isOpen: true,
          message: 'Error creating employee!',
        });
      }
    } finally {
      setName('');
      setSalary('');
      setAge('');
    }
  };

  const closeDialog = () => {
    setDialog({ isOpen: false, message: '' });
  };

  const handleRefresh = () => {
    window.location.reload();
  };

  return (
    <div className="inputContainer">
      <form onSubmit={handleSubmit}>
        <div className="inputRow">
          <label className="inputContainerLabel">Name:</label>
          <input type="text" className="inputContainerInput" value={name} onChange={(e) => setName(e.target.value)} />
        </div>
        <div className="inputRow">
          <label className="inputContainerLabel">Salary:</label>
          <input type="text" className="inputContainerInput" value={salary} onChange={(e) => setSalary(e.target.value)} />
        </div>
        <div className="inputRow">
          <label className="inputContainerLabel">Age:</label>
          <input type="text" className="inputContainerInput" value={age} onChange={(e) => setAge(e.target.value)} />
        </div>
        <div className="buttonContainer">
          <button className="buttonContainerButton" onClick={handleRefresh}>
            Back
          </button>
          <button className="buttonContainerButton" type="submit">Create Employee</button>
        </div>
      </form>
  
      {dialog.isOpen && (
        <div className="dialogContainer">
          <p className="dialogContainerP">{dialog.message}</p>
          <button className="dialogContainerButton" onClick={closeDialog}>Close</button>
        </div>
      )}
    </div>
  );
  
};

export default EmployeeCreate;
