import React, { useState, useEffect } from 'react';
import axios from '../lib/axios';

const EmployeeUpdate = ({ selectedEmployee, closeModal, onSave, setIsEditing }) => {
  const [editedEmployee, setEditedEmployee] = useState(selectedEmployee);
  const [dialog, setDialog] = useState({ isOpen: false, message: '' });

  useEffect(() => {
    setEditedEmployee(selectedEmployee);
  }, [selectedEmployee]);

  const handleNameChange = (e) => {
    setEditedEmployee({ ...editedEmployee, employee_name: e.target.value });
  };

  const handleAgeChange = (e) => {
    setEditedEmployee({ ...editedEmployee, employee_age: e.target.value });
  };

  const handleSalaryChange = (e) => {
    setEditedEmployee({ ...editedEmployee, employee_salary: e.target.value });
  };

  const handleSave = async () => {
    try {
      const updatingEmployee = {
        name: editedEmployee.employee_name,
        salary: editedEmployee.employee_salary,
        age: editedEmployee.employee_age,
      };

      await axios.put(`/update/${selectedEmployee.id}`, updatingEmployee);
      setDialog({
        isOpen: true,
        message: 'Employee updated successfully!',
      });
      console.log('Employee created:', response.data);
      onSave(editedEmployee);
    } catch (error) {
      console.error(error);
      if (error) {
        setDialog({
          isOpen: true,
          message: 'Error updating employee!',
        });
      }
    }finally {
      setTimeout(closeDialog, 3000);
    }
  };

  const closeDialog = () => {
    setDialog({ isOpen: false, message: '' });
  };

  return (
    <div style={styles.modal}>
      <div style={styles.modalContent}>
        <button onClick={closeModal} style={styles.closeButton}>X</button>
        <div style={{ ...styles.inputRow, marginTop: '5%' }}>
            <h2>{editedEmployee.employee_name}</h2>
          </div>
        <div style={styles.inputContainer}>
          <div style={styles.inputRow}>
            <label style={styles.inputContainerLabel}>Name:</label>
            <input
              style={styles.inputContainerInput}
              type="text"
              value={editedEmployee.employee_name}
              onChange={handleNameChange}
            />
          </div>
          <div style={styles.inputRow}>
            <label style={styles.inputContainerLabel}>Age:</label>
            <input
              style={styles.inputContainerInput}
              type="number"
              value={editedEmployee.employee_age}
              onChange={handleAgeChange}
            />
          </div>
          <div style={styles.inputRow}>
            <label style={styles.inputContainerLabel}>Salary:</label>
            <input
              style={styles.inputContainerInput}
              type="number"
              value={editedEmployee.employee_salary}
              onChange={handleSalaryChange}
            />
          </div>
          <div style={styles.buttonContainer}>
            <button
              style={{ ...styles.buttonContainerButton, backgroundColor: 'grey' }}
              onClick={() => setIsEditing(false)}
            >
              Cancel
            </button>
            <button
              style={styles.buttonContainerButton}
              onClick={handleSave}
            >
              Save
            </button>
          </div>
        </div>
      </div>
      {dialog.isOpen && (
        <div className="dialogContainer">
          <p className="dialogContainerP">{dialog.message}</p>
          <button className="dialogContainerButton" onClick={closeDialog}>Close</button>
        </div>
      )}
    </div>
  );
};

const styles = {
  modal: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    backgroundColor: '#fff',
    padding: '20px',
    borderRadius: '8px',
    position: 'relative',
    width: '30%',
    height: '40%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  closeButton: {
    position: 'absolute',
    top: '10px',
    right: '10px',
    backgroundColor: 'transparent',
    border: 'none',
    fontSize: '20px',
    cursor: 'pointer',
  },
  buttonContainer: {
    marginTop: '20px',
    textAlign: 'center',
  },
  buttonContainerButton: {
    padding: '10px 20px',
    fontSize: '16px',
    backgroundColor: '#007bff',
    color: 'white',
    border: 'none',
    borderRadius: '4px',
    cursor: 'pointer',
    marginRight: '10px',
  },
  inputContainer: {
    marginTop: '15%',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  inputRow: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '10px',
  },
  inputContainerLabel: {
    display: 'inline-block',
    width: '100px',
    marginRight: '10px',
    fontWeight: 'bold',
  },
  inputContainerInput: {
    width: '60%',
    maxWidth: '300px',
    padding: '8px',
    fontSize: '16px',
    border: '1px solid #ccc',
    borderRadius: '4px',
  },
};

export default EmployeeUpdate;
