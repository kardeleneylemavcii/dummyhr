"use client";

import React, { useContext } from "react";
import "../css/Header.css";
import { AppBar, Toolbar, Typography, Grid } from "@mui/material";

const Header = () => {

  return (
    <AppBar className="custom-header">
      <Toolbar className="custom-nav">
          <>
            <Grid container spacing={2} style={{ display: "flex", flexDirection: "row" ,justifyContent:"space-between"}}>
              <Grid item style={{margin:10}}>
                <Typography variant="h5" style={{ fontWeight: "bold", fontStyle: "italic" }}>
                  Dummy İnsan Kaynakları
                </Typography>
              </Grid>
            </Grid>
          </>
      </Toolbar>
    </AppBar>

  );
};

export default Header;